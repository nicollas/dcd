<?php
$servername = "localhost";
$username = "root";
$password = "usbw";
$dbname = "coop";

// Cria a conexão
$conn = new mysqli($servername, $username, $password, $dbname);
// Checa a conexão
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (!$link = mysql_connect('localhost', 'root', 'usbw', 'coop')) {
    echo 'Não foi possível conectar ao mysql';
	echo 'Erro MySQL: ' . mysql_error();
    exit;
}

mysql_select_db("coop") or die(mysql_error());

$result = mysql_query("SELECT localizacao FROM agentea WHERE ID = 1");
$dados = mysql_fetch_row($result);
if (!$result) {
    echo "Erro do banco de dados, não foi possível consultar o banco de dados\n";
    echo 'Erro MySQL: ' . mysql_error();
    exit;
}

$conn->close();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>PBH</title>
	<style>
      #map {
        height: 500px;
        width: 100%;
       }
    </style>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
  </head>
  <body>
  <div class="container">
	<br>
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
	  <li class="nav-item">
		<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Dashboard</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Abrir ocorrência</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Fechar ocorrência</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" id="pills-dois-tab" data-toggle="pill" href="#pills-dois" role="tab" aria-controls="pills-dois" aria-selected="false">Cadastrar agente</a>
	  </li>
	</ul>
	<div class="tab-content" id="pills-tabContent">
	  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<div>
			<div class="row">
				<div class="col">
					<form>
						<label for="exampleFormControlSelect1">Tipo de agente</label>
						<select class="form-control" id="exampleFormControlSelect1">
						  <option>Bombeiro</option>
						  <option>Guarda municipal</option>
						  <option>Policial miliar</option>
						  <option>SAMU</option>
						</select>
					</form>
				</div>
				<div class="col">
					<form>
						<label for="exampleFormControlSelect1">Tipo de ocorrência</label>
						<select class="form-control" id="exampleFormControlSelect1">
						  <option>Acidente</option>
						  <option>Animal na pista</option>
						  <option>Crime</option>
						</select>
					</form>
				</div>
		    </div>
			<br>
			<h3>Agentes em campo</h3>
			<div id="map"></div>
			<script>
			  function initMap() {
				var uluru = {lat: -19.8722363, lng: -43.9365958}; //"<?php echo $dados[0]; ?>"; 
				var map = new google.maps.Map(document.getElementById('map'), {
				  zoom: 12,
				  center: uluru
				});
				var marker = new google.maps.Marker({
				  position: uluru,
				  map: map
				});
			  }
			</script>
			<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-BmP8vumIsryliXok_4HJWO5uHHHJMcs&callback=initMap">
			</script>
		</div>
	  </div>
	  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
		<form>
			<label for="exampleFormControlSelect1">Tipo de ocorrência</label>
			<select class="form-control" id="exampleFormControlSelect1">
				<option>Acidente</option>
				<option>Animal na pista</option>
				<option>Crime</option>
				<option>SAMU</option>
			</select>
			<br>
			<div class="row">
				<div class="col">
					<label for="formGroupExampleInput">Estado</label>
					<select class="form-control" id="exampleFormControlSelect1">
						<option>Minas gerais</option>
						<option>São Paulo</option>
						<option>Rio de Janeiro</option>
					</select>
				</div>
				<div class="col">
					<label for="formGroupExampleInput">Cidade</label>
					<select class="form-control" id="exampleFormControlSelect1">
						<option>Belo Horizonte</option>
						<option>Lagoa Santa</option>
						<option>Santa Luzia</option>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col">
					<label for="formGroupExampleInput">Bairro</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Bairro">
				</div>
				<div class="col">
					<label for="formGroupExampleInput">Av/Rua</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Av/Rua">
				</div>
				<div class="col">
					<label for="formGroupExampleInput">Número</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Número">
				</div>
				<div class="col">
					<label for="formGroupExampleInput">Complemento</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Complemento">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col">
					<label for="formGroupExampleInput">Data de início</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="data de início">
				</div>
				<div class="col">
					<label for="formGroupExampleInput">Hora de início</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="hora de início">
				</div>
			</div>
			<br>
			<label for="exampleFormControlTextarea1">Descrição da ocorrência</label>
			<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
			<br>
			<button type="button" class="btn btn-success">Abrir ocorrência</button>
		</form>
	  </div>
	  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
	  <form>
			<div class="row">
				<div class="col">
					<label for="formGroupExampleInput">ID da ocorrência</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="ID">
				</div>
				<div class="col">
					<label for="formGroupExampleInput">Mensagem de fechamento</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Mensagem de fechamento">
				</div>
			</div>
			<br>
			<button type="text" class="btn btn-primary" aria-label="Large" aria-describedby="inputGroup-sizing-sm">Fechar ocorrência</button>
		</form>
	  </div>
	  <div class="tab-pane fade" id="pills-dois" role="tabpanel" aria-labelledby="pills-dois-tab">
		<form>
		<h1>Cadastro de Agente</h1>
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Nome</span>
  </div>
  <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
</div>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Tipo</span>
  </div>
  <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
</div>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">contato</span>
  </div>
  <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
</div>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">localização</span>
  </div>
  <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
</div>

<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">status</span>
  </div>
  <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
</div>
  <button type="text" class="btn btn-primary" aria-label="Large" aria-describedby="inputGroup-sizing-sm"> Enviar</button>
  </form>

	  </div>
	</div>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>